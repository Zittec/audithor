# AudiThor

Software libre para control de proyectos, compras, ventas, inventarios, tienda virtual y más, con tecnología cliente-servidor (XAMPP).

## Comenzar

Este software se encuentra aún en una fase pre-alpha por lo que no tiene ningún instalador aún. Se están definiendo aún muchas partes del sistema por lo que debes hacer una instalación manual si quieres probarlo.

### Prerequisites

XAMP (Linux/Windows + Apache + MySQL + PHP)

```
sudo apt-get install apache2 mysql-server mysql-client php libapache2-mod-php php-mysql
```

### Instalación

(PRÓXIMAMENTE) Paso por paso hasta que esté funcionando...

Paso 1

```
Clonar repositorio
```

Paso 2

```
Editar archivo de configuración para la conexión a la base de datos
```

Paso 3

```
Iniciar sesión con el usuario audithor y la contraseña audithor
```

Listo!

## Ejecutando y probando

(PRÓXIMAMENTE) Eplicaré cómo ejecutar y efectuar pruebas de funcionamiento

### 1 PRUEBA1

(PRÓXIMAMENTE) Explica qué verifican estas pruebas y por qué

```
(PRÓXIMAMENTE) Ejemplo 1
```

### 2 PRUEBA2

(PRÓXIMAMENTE) Explica qué verifican estas pruebas y por qué

```
(PRÓXIMAMENTE) Ejemplo 2
```

## Implantación

(PRÓXIMAMENTE) cómo implementar esto en un sistema en uso??

## Construido con

* [PHP](https://www.php.net/) - Lenguaje de programación
* [SMARTY](https://www.smarty.net/) - Motor de plantillas

## Contribuir

Apenas voy a crear el [CONTRIBUTING.md](https://gitlab.com/zittec/) donde podrás ver detalles en nuestra conducta de código y el proceso para enviarnos un "pull request".

## Versiones

Usamos [SemVer](http://semver.org/) para las versiones.

## Autores

* **Lucio Chávez** - *Trabajo inicial* - [@luciochavez](https://gitlab.com/luciochavez)

Ve también esta lista de personas [participantes](https://gitlab.com/Zittec/AudiThor/graphs/master) que han participado en este proyecto.

## Licencia

Este proyecto está liberado bajo una licencia GPLv3 - ve [LICENSE.md](LICENSE.md) para más detalles.

## Agradecimientos

* A mi familia que me ha apoyado en la locura de realizar este proyecto.
